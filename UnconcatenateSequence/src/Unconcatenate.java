import java.util.HashMap;


public class Unconcatenate {
	
	HashMap<String,Integer> dict = new HashMap<String,Integer>();
	
	public Unconcatenate(){
		dict.put("this", 1);
		dict.put("is", 1);
		dict.put("awesome", 1);
		dict.put("a", 1);
		dict.put("we", 1);
	}
	
	public String uncat(String str){
		char[] chrArry = str.toCharArray();
		char[] newChrArry = new char[chrArry.length +1];
		newChrArry[0] = ' ';
		for( int i = 0 ; i < chrArry.length;i++)
			newChrArry[i+1] = chrArry[i];
		int[] opt = new int[newChrArry.length];
		int[] prev = new int[newChrArry.length ];
		opt[0] = 0;
		prev[0] = -1;
		for(int i= 1; i < newChrArry.length;i++){
			int j = max(opt,i-1,newChrArry);
			opt[i] = opt[j] + quality(j+1,i, newChrArry);
			if(i > 0)
				prev[i] = j;
		}
		
		String finalStr = "";
		int max =Integer.MIN_VALUE;
		int index = 0;
		for(int i=0;i<newChrArry.length;i++){
			if(opt[i] > max){
				max = opt[i];
				index = i;
			}
		}
		finalStr += toString(newChrArry,index + 1, newChrArry.length-1) + " ";
		while(index!= -1){
			finalStr += toString(newChrArry,prev[index]+1, index) + " ";
			index = prev[index];
		}
		return finalStr;
	}
	public String toString(char[] newChrArry,int j,int i){
		char[] arr = new char[i-j + 1];
		String str = "";
		for(int k =0;k<arr.length;k++)
			str+= newChrArry[j+k];
		return str; // using char.toString  which returned the hex address of the char array
	}
	public int quality(int j,int i, char[] newCharArry){
		char[] newChr =  new char[i- j + 1];
		String str = "";
		for(int k =0;k< newChr.length;k++){
			str+= newCharArry[j+k];
		}
		
	//	System.out.println(str);
		if(dict.containsKey(str))
			return dict.get(str);
		else
			return -1;
	}
	
	public int max(int[] opt,int i, char[] newCharArry){
		int max = Integer.MIN_VALUE;
		int index =0 ;
		for(int j=i; j >=0; j--){
			if(quality(j+1,i+1,newCharArry) + opt[j] > max){ // mistake sending i instead of i+1, quality array never got initialized
				max = opt[j] + quality(j+1,i+1,newCharArry);
				index = j;
			}
		}
		return index;
	}
}
