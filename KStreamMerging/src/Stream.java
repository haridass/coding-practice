
public interface Stream {
	public boolean hasNext();
	public int next();
	public void add(int value);
	public void printList();
	public void initIterator();

}
