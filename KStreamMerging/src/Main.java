import java.util.ArrayList;


public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StreamClass s1 = new StreamClass();
		s1.add(1); s1.add(4);
		StreamClass s2 = new StreamClass();
		s2.add(5); s2.add(12);
		StreamClass s3 = new StreamClass();
		s3.add(3); s3.add(6); s3.add(15);
		ArrayList<Stream> al = new ArrayList<Stream>();
		al.add(s1); al.add(s2); al.add(s3);
		s1.initIterator();
		s2.initIterator();
		s3.initIterator();
		MergeStreams m = new MergeStreams();
		Stream op = m.merge(al);
		StreamClass opStr = (StreamClass)op;
		op.printList();

	}

}
