import java.util.ArrayList;
import java.util.PriorityQueue;


public class MergeStreams {
	public Stream merge(ArrayList<Stream> streams){
		Stream opStream = new StreamClass();
		//int[] indices = new int[streams.size()];
		PriorityQueue<Node> min = new PriorityQueue<Node>();
		for(int i =0; i<streams.size();i++){
			Object obj = streams.get(i);
			System.out.println(streams.get(i));
			if(streams.get(i).hasNext()){
				System.out.println("reached");
				int entry = streams.get(i).next();
				Node n = new  Node(entry,i);
				min.add(n);
			}
		}
		
		while(!min.isEmpty()){
			Node n = min.poll();
			opStream.add(n.value);
			if(streams.get(n.index).hasNext()){
				Node newN = new Node(streams.get(n.index).next(),n.index);
				min.add(newN);
			}
		}
		
		
		return opStream;
		
	}
	
	

}
