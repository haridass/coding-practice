
public class Node implements Comparable {

	int value = 0;
	int index = 0;
	Node(int val,int ind){
		this.value = val;
		this.index = ind;
	}
	@Override
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		Node n = (Node)arg0;
		
		return this.value - n.value;
	}

}
