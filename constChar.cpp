#include <stdio.h>
#include <iostream>

int main(){
	char a[4] ={'a','b','c','d'};
	char * const p = a;
	char b ='b';
//	p=&b; //ERROR p is a constant pointer to a hence cannot point to b's location.
	for(int i=0;i<4;i++){
		p[i] = 'e'; //p can modify a 
	}
	printf("%s\n",a);
	const char * c = a;
//	c = &b; C can point to different location.

	//ERROR c cannot change th data of a
//	for(int i=0;i<4;i++){
//		c[i] = 'n';
//	}
	printf("%s\n",c);
}
