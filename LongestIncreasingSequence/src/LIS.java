import java.util.ArrayList;


public class LIS {
	
	public ArrayList<Integer> getLIS(Integer[] seq){
		Integer[] opt = new Integer[seq.length];
		Integer[] prev = new Integer[seq.length];
		prev[0] = -1;
		opt[0] =1;
		ArrayList<Integer> finalSeq = new ArrayList<Integer>();
		for(int i=1; i < seq.length; i++){
			int max =0;
			int index = -1;
			for(int j= i-1;j>= 0; j--){
				if( seq[j] < seq[i] && opt[j] > max){
					index = j;
					max = opt[j];
				}
			}
			prev[i] = index;
			opt[i] = max + 1;
		}
		int max =0;
		int index = -1;
		for(int i =0 ; i < seq.length; i++){
			if(opt[i] > max ){
				max = opt[i];
				index = i;
			}	
		}
		
		while(index != -1){
			finalSeq.add(seq[index]);
			index = prev[index];
		}
		
		return finalSeq;
	}

}
