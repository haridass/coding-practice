
public class Node {
	int type;
	int pos;
	Node next;
	Node(int type, int pos){
		this.type = type;
		this.pos = pos;
		this.next = null;
	}
}
