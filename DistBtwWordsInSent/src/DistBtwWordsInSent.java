import java.util.ArrayList;


public class DistBtwWordsInSent {
	
	public static Node mergeList(Node listA,Node listB){
		Node ptrA = listA;
		Node ptrB = listB;
		Node newList = null,ptr;
		if(listA.pos <listB.pos){
			newList = listA;
			ptr = newList;
			ptrA= ptrA.next;
		}else{
			newList = listB;
			ptr = newList;
			ptrB = ptrB.next;
		}
		while(ptrA != null && ptrB != null){
			if(ptrA.pos < ptrB.pos){
				ptr.next = ptrA;
				ptrA = ptrA.next;
			}
			else{
				ptr.next = ptrB;
				ptrB = ptrB.next;
			}
			ptr = ptr.next;
		}
		if(ptrB == null)
			ptr.next = ptrA;
		else
			ptr.next = ptrB;
		return newList;
		
	}
	public static int compareDist(Node list){
		int curr;
		int prev = -1;
		int prevType = 0;
		int currType;
		Node ptr = list;
		int min = Integer.MAX_VALUE;
		while(ptr!= null){
			curr = ptr.pos;
			if(prev != -1){
				if(ptr.type != prevType){
					int dist = Math.abs(curr - prev);
					if(dist < min){
						min = dist;
					}
				}
			}
			prev = curr;
			prevType = ptr.type;
			ptr = ptr.next;
		}
		return min;
	}
	
	public static void compareWithLists(){

		// TODO Auto-generated method stub
		String sent = "THis city has been philadelphia philadelphia iasdf asdf asdi asd The city of  asdf asdf dsaf asd fsd fasd fasdf asdf philadelphia";
		Node listA = null;
		Node ptrA = listA;
		Node listB = null;
		Node ptrB = listB;

		String[] strArry = sent.split(" ");
		for(int i =0; i < strArry.length ; i++){
			if((strArry[i]).equalsIgnoreCase("philadelphia")){
				Node n = new Node(1,i);
				if(listA == null){
					listA = n;
					ptrA = listA;
				}else{
					ptrA.next = n;
					ptrA = ptrA.next;
				}	
			}
			if((strArry[i]).equalsIgnoreCase("city")){
				Node n = new Node(2,i);
				if(listB == null){
					listB = n;
					ptrB = listB;
				}else{
					ptrB.next = n;
					ptrB = ptrB.next;
				}
			}
		}
		
		Node newList = mergeList(listA,listB);
		System.out.println(compareDist(newList));
	
	}
	
	public static void simpleComparision(){
		String passage = "THis city has been philadelphia philadelphia iasdf asdf asdi asd The city of  asdf asdf dsaf asd fsd fasd fasdf asdf philadelphia";
		int counter1 = -1;
		int counter2 = -1;
		String[] strArry = passage.split(" ");
		int dist = Integer.MAX_VALUE;
		
		for(int i =0;i < strArry.length;i++){
			if(strArry[i].equalsIgnoreCase("philadelphia")){
				counter1 = i;
				if(counter2 >= 0){
					int temp = Math.abs(counter1 - counter2);
					if(temp < dist)
						dist = temp;
				}
			}
			
			if(strArry[i].equalsIgnoreCase("city")){
				counter2 = i;
				if(counter1 >= 0){
					int temp = Math.abs(counter2 - counter1);
					if(temp < dist)
						dist = temp;
				}
			}
		}
		System.out.println(dist);
	}
	public static void main(String[] args) {
		//DistBtwWordsInSent.compareWithLists();
		DistBtwWordsInSent.simpleComparision();
	}
}
