
public class Node  implements Comparable{
	int nodeData;
	Node(int data){
		nodeData = data;
	}
	@Override
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		Node n = (Node)arg0;
		
		return (this.nodeData - n.nodeData);
	}

}
