import java.util.HashMap;


public class ExcelNum2Name {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int inputNum = Integer.parseInt(args[0]);
		String convertedNum = "";
		int divident = inputNum;
		int quotient = 0;
		int remainder = 0;
		
		createMapping();
		/**********************************************************
		 * This is similar to converting a decimal number to binary
		 * successively dividing the number and concatenating the
		 * remainders form the first
		 ***********************************************************/
		while(divident/26 != 0){
			quotient = divident/26;
			remainder = divident%26;
			convertedNum  += charMap.get(remainder);
			divident = quotient;
		}
		quotient = divident/26;
		remainder = divident%26;
		convertedNum  = charMap.get(remainder) + convertedNum;
		System.out.println(convertedNum);

	}
	
	public static void createMapping(){
		for(int i=1;i <26;i++){
			charMap.put(i, (char) (i+65-1));
		}
		/********************************************************
		*Any number %26 gives number less than 26, but it is 
		*zero for multiples of 26. Hence 0 should be mapped to Z
		*********************************************************/
		charMap.put(0, 'Z');
	}
	
	private static HashMap<Integer,Character> charMap = new HashMap<Integer,Character>();
}
