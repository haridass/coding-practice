
public class FibonacciRecurssive {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(fibonacci(5));
	}
	
	public static  int fibonacci(int n){
		if(n == 0 || n== 1){
			return 1;
		}
		if(n<0)
			return 0;
		
		 return fibonacci(n-1) + fibonacci(n-2);
	}

}
