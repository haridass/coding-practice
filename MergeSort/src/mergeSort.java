
public class mergeSort {
	public int[] sort(int[] array){
		if(array == null)
			return null;
		if(array.length ==1)
			return array;
		if(array.length == 2){
			if(array[0] < array[1])
				return array;
			else{
				swap(array);
				return array;
			}
		}
		int mid = (array.length)/2;
		int[] left = copySubArray(array,0,mid);
		int[] right = copySubArray(array,mid,array.length);
		int[] leftArr = sort(left);
		int[] rghtArr = sort(right);
		return merge(leftArr,rghtArr);
	}
	public void swap(int[] array){
		int temp = array[0];
		array[0] = array[1];
		array[1] = temp;
	}
	public int[] copySubArray(int[] array,int strt, int end){
		int[] arr = new int[end- strt];
		for(int i=0;strt<end;i++,strt++){
			arr[i] = array[strt];
		}
		return arr;
	}
	public int[] merge(int[] left, int[] right){
		int[] arr = new int[left.length + right.length];
		int i=0,k=0,j=0;
		while(k < left.length && j < right.length){
			if(left[k] < right[j]){
				arr[i] = left[k];
				k++;
			}else{
				arr[i] = right[j];
				j++;
			}
			i++;
		}
		if(k<left.length){
			while(k<left.length){
				arr[i]= left[k];
				i++;
				k++;
			}
			
		}else{
			while(j<right.length){
				arr[i]= right[j];
				i++;
				j++;
			}
		}
		
		return arr;
	}

}
