import java.util.ArrayList;
import java.util.Comparator;


public class Node implements Comparable{
	
	Node[] children = new Node[3]; 
	int data;
	
	public Node(int data){
		for(int i=0;i<3;i++)
			children[i] = null;
		this.data = data;
	}
	
	@Override
	public int compareTo(Object arg0) {
		// TODO Auto-generated method stub
		Node obj = (Node)arg0;
		return this.data -obj.data;
	}

}
