import java.util.ArrayList;


public class Ancestor {
	
	public Result findAncestor(Node n,Node p,Node q){
		Result res = new Result(false,null);
		Result tempRes = new Result(false,null);
		if(n == null)
			return res;
		if(n.data == p.data && n.data == q.data){
			res.ancsFound = true;
			res.node = n;
		}
	//	if(n.data == p.data || n.data == q.data)
	//		res.node = n;

			for(int i =0;i<3;i++){
				tempRes = findAncestor(n.children[i],p,q);
				if(tempRes.ancsFound){
					return tempRes;
				}
					// when the current node is one of p or q and p or q was found at one of the earlier child nodes
				if(n.data == p.data || n.data == q.data){
					if(tempRes.node != null){
						res.node = n;
						res.ancsFound = true;	
					}else{
						res.node = n;
					}
				}else{
 					if(tempRes.node != null){
 						//When the both the node are in one of the child nodes
						if(res.node != null){
							res.ancsFound = true;
							res.node = n;
						}else
							res = tempRes;
					}
					
				}
		}
//			if(res.node != null)
//				System.out.println(res.node.data);
		return res;
	
	}

	
	public Node root;
	
	public void createTree(){
		root = new Node(0);
		root.children[0] = (new Node(1));
		root.children[1] = (new Node(2));
		root.children[2] = (new Node(3));

		root.children[2].children[0] = new Node(4);
		root.children[2].children[1] = new Node(5);
		root.children[2].children[2] = new Node(6);
		root.children[1].children[0] = new Node(7);
		root.children[1].children[0].children[0] = new Node(8);
	}
	
	public void inorder(Node node){
		if(node == null)
			return;
		System.out.println(node.data);
		for(int i =0;i<3;i++){
				inorder(node.children[i]);
			}
		}
}
