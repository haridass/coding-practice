#include <stdio.h>
#include <iostream>

#define NUMALPA 26
class Trie{
	public:
	Trie();
	~Trie();
	void addWord(std::string,std::string);
	void autoComplete(std::string);
	int getIndex(char);
	Trie *childNode[NUMALPA];
	std::string key;
	bool isLeaf;
};

Trie::Trie()
	:isLeaf(false){
	for(int i=0;i<NUMALPA;i++){
		childNode[i] = NULL;
		
	}
}
int
Trie::getIndex(char ch){
	return (int)(ch - 'a');
}
void
Trie::addWord(std::string word,std::string key){
	if(word ==""){
		isLeaf = true;
		this->key = key;
	}
	else{
		int index = getIndex(word[0]);
		if(childNode[index] == NULL)
			childNode[index] = new Trie();
		childNode[index]->addWord(word.substr(1),key+word[0]);
	}
}

/*************************************************************
*Traverse till the leave node and print the key values at the 
leaf node. Traverse the Trie till the word matches the Trie 
elements and then traverse all non NULL nodes till  the leaf
node.
*************************************************************/
void 
Trie::autoComplete(std::string word){
	if(isLeaf){
		std::cout << key << std::endl;
		return;
	}
	else{
		if(word == ""){
			for(int i=0;i<NUMALPA ;i++)
				if(childNode[i] != NULL)
					childNode[i]->autoComplete("");
		}
		else{
			int index =getIndex(word[0]);
			if(childNode[index]!=NULL)
				childNode[index]->autoComplete(word.substr(1));
		}
	}
	
	
}

int main(){
	Trie *root = new Trie();
	root->addWord("abcd","");
	root->addWord("abef","");
	root->addWord("alphabet","");
	root->addWord("word","");
	root->autoComplete("ab");
	return 0;
}			
