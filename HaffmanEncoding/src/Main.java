import java.util.HashMap;
import java.util.PriorityQueue;


public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str = readLineFromFile();
		int[] charFreq = new int[256];
		char[] line = str.toCharArray();
		for(int i=0; i< line.length;i++){
			/*Increasing the frequency of the character read*/
			charFreq[line[i]]++;			
		}
		PriorityQueue<Node> pq = new PriorityQueue<Node>();
		/********************************************************
		 * Loop over the character's array and add all characters
		 * with non-zero frequencies
		 **************************************************/
		for(int i=0;i< 256;i++){
			if(charFreq[i] != 0 ){
				/*****************************************
				 * Only add nodes that have positive frequency
				 ****************************************/
				Node n = new Node((char)i+"",charFreq[i]);
				pq.add(n);
			}
		}

		//System.out.println(pq.size());
		while(pq.size()>1){
			
			Node rightChild = pq.remove();
			//System.out.println(leftChild.ch + "\t" + leftChild.freq);
			Node leftChild = pq.remove();
			//System.out.println(rightChild.ch + "\t" + leftChild.freq);
			Node parent = new Node("",leftChild.freq+rightChild.freq);
			parent.leftChild = leftChild;
			parent.rightChild = rightChild;
		//	System.out.println(parent.ch + "\t" + parent.freq);
			pq.add(parent);
		}
		//System.out.println(pq.size());
		Node root = pq.remove();
		/********************************************
		 * Code to assign encoding code to characters
		 ********************************************/
		encoding = new HashMap<String,String>();
		traverse(root,"");
		
		/******************************
		 * Print out the encoding
		 ****************************/
		System.out.println(encoding);
	}
	
	static String readLineFromFile(){
		return "aaaabbbcddccdccdccdcieie";
		
	}
	static void traverse(Node node,String s){
		/*************************************
		 * Traverse the created tree assigning
		 *  0 for leftChild
		 *  1 for rightChild
		 *  If it is leaf node with character
		 *  store the String so far in hashMap
		 **************************************/
		//System.out.println(node.freq + "\t" + s + "\t" + node.ch);
		if(!node.ch.equalsIgnoreCase("")){
			encoding.put(node.ch,s);
			return;   
		}
		traverse(node.leftChild,s+"0");
		traverse(node.rightChild,s+"1");
	}
	public static HashMap<String,String> encoding;

}
