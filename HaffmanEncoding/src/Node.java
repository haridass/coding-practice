
public class Node implements Comparable {
	String ch;
	int freq;
	Node leftChild;
	Node rightChild;
	public boolean hasChar;
	Node(String ch,int freq){
		this.ch = ch;
		this.freq = freq;
		leftChild = null;
		rightChild = null;
	}
	@Override
	public int compareTo(Object obj) {
		// TODO Auto-generated method stub
		Node n = (Node)obj;
		
		return (this.freq - n.freq);
		//How is this implemented 
	}

}
