import java.util.ArrayList;
import java.util.HashSet;
import java.util.Stack;


public class Graph {
	ArrayList<Integer>[] adjList;
	HashSet<Integer> finishedVertex = new HashSet<Integer>();
	HashSet<Integer> visitedVertex = new HashSet<Integer>();
	Stack<Integer> stk = new Stack<Integer>();
	Graph(ArrayList<Integer>[] adjList){
		this.adjList = adjList;		
                                                   	}
	
public boolean chkCycles(){
	boolean isCycle = false;
	
	//Traverse the adjList and pick up one element at time
	stk.push(0);
	while(!stk.empty()){
		int vertex = stk.pop();
		if(visitedVertex.contains(vertex)){
			if(!finishedVertex.contains(vertex)){
				isCycle = true;
				return isCycle;
			}
		}else{
		ArrayList<Integer> adjVertices = adjList[vertex];
		for(int i:adjVertices){
				stk.push(i);
				visitedVertex.add(i);
			}
		}
		finishedVertex.add(vertex);
	}
	
	return isCycle;
}
	
}
