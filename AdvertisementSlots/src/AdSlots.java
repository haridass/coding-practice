
import java.util.*;
public class AdSlots {

	public static int maxRevenue = Integer.MIN_VALUE;
	public static ArrayList<Node> finalList;
	
	public static HashMap<String,Integer>  m_slots = new HashMap<String,Integer>();
	public static HashMap<String,Integer> m_ads = new HashMap<String,Integer>();
    public static HashMap<String,ArrayList<String>> m_slotsAds = new HashMap<String,ArrayList<String>>();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		m_ads.put("c001", 50);
		m_ads.put("c002", 5);
		m_ads.put("c003", 15);
		m_ads.put("empty", 0);
		m_slots.put("ct", 10);
		m_slots.put("cm", 20);
		
		ArrayList<String> adsList = new ArrayList<String>();
		adsList.add("c001");
		adsList.add("c002");
		adsList.add("c003");
		m_slotsAds.put("ct",adsList);
		adsList = new ArrayList<String>();
		adsList.add("c001");
		adsList.add("c002");
		adsList.add("empty");
		m_slotsAds.put("cm",adsList);

		ArrayList<String> slotsList = new ArrayList<String>();
		slotsList.add("cm"); slotsList.add("ct");
		ArrayList<Node> productsList = new ArrayList<Node>();
		HashSet<String> usedAds = new HashSet<String>();
		recurse(slotsList,productsList,usedAds);
		System.out.println(maxRevenue);

	}
	
	public static void recurse(ArrayList<String> slotsList, ArrayList<Node> productsList,HashSet<String> usedAds){
		if(slotsList.size() ==0){
			int sum =0;
			for(Node node:productsList){
				System.out.print("slot :: " + node.slot);
				System.out.print("\t" + "ad :: " + node.ad  + "\t");
				sum = sum + m_slots.get(node.slot) * m_ads.get(node.ad);
			}
			System.out.println("\t" + sum);
			if(sum > maxRevenue){
				maxRevenue = sum;
				finalList = productsList;
			}
			return;
		}

		// make copies of every array list
		ArrayList<String> slotsListCpy = (ArrayList<String>) slotsList.clone();
		ArrayList<Node> productsListCpy = (ArrayList<Node>)productsList.clone();
		HashSet<String> usedsAdsCpy = (HashSet<String> ) usedAds.clone();
		
		String slot = slotsListCpy.remove(slotsListCpy.size()-1);
		ArrayList<String> adsList = m_slotsAds.get(slot);
		for(String ad:adsList){
			if(!usedsAdsCpy.contains(ad)){
				usedsAdsCpy.add(ad);
				Node node = new Node(slot,ad);
				productsListCpy = (ArrayList<Node>)productsList.clone();
				productsListCpy.add(node);
				recurse(slotsListCpy,productsListCpy,usedsAdsCpy);
				usedsAdsCpy.remove(ad);
			}
		}
	}

}
