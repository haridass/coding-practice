
public class fib {
    public static void main(String[] args) {
        System.out.println(fibonacci(8));
}
public static int fibonacci(int n){
        if(n == 0 || n==1){
                return 1;
        }
        int prev = 1;
        int prevOfPrev = 1;
        int sum = prev, i= 1;
        while(i < n){
                sum = prev + prevOfPrev;
                prevOfPrev = prev;
                prev =sum;
                i++;
        }
        return sum;
}
	

}
